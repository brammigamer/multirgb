#include <Arduino.h>

void writeMUX(int a, int b, int c)
{
  Serial.print(a);
  Serial.print(" ");
  Serial.print(b);
  Serial.print(" ");
  Serial.println(c);

  digitalWrite(2, a);
  digitalWrite(4, b);
  digitalWrite(12, c);
}

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(2, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(32, INPUT);

  writeMUX(1, 0, 1);
}

void loop()
{
  // put your main code here, to run repeatedly:


  // for (int i = 0; i < 8; i++)
  // {
  //   writeMUX(i & 1, (i >> 1) & 1, (i >> 2) & 1);
  //   delay(100);
  //   Serial.print(i);
  //   Serial.print(" -- ");
  //   delay(250);
    float a = analogRead(32);
    Serial.println(a);
    delay(10);

  // }


  delay(500);
}
